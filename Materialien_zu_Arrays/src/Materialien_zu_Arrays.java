
import java.util.Scanner;
public class Materialien_zu_Arrays {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		/*Arbeitsblatt
		 * AS Arrays
		*/
		
		//Aufgabe 1
		
		// 1. Erzeugen Sie ein Array namens myArray, das 10 Werte speichern kann.
		int [] myArray = new int [10];
		char wahl;
		
		//2. Speichern Sie 10 beliebige Werte in Ihrem Array. 
		myArray[0] = 100;
		myArray[1] = 110;
		myArray[2] = 120;
		myArray[3] = 130;
		myArray[4] = 140;
		myArray[5] = 150;
		myArray[6] = 160;
		myArray[7] = 170;
		myArray[8] = 180;
		myArray[9] = 190;
		
		//3. Lassen Sie mit Hilfe einer for-Schleife die 10 Werte ausgeben.
		for (int i = 0; i <	 myArray.length; i++) {
			System.out.println(myArray[i]);
		}
		
		//4. Speichern Sie am Index 3 den Wert 1000.
		myArray[2] = 1000;
		
		//5. Lassen Sie zum �berpr�fen den Wert des Index 3 ausgeben
		System.out.println (myArray[2]);
		
		//6. �berschrieben Sie alle Werte mit Hilfe einer for-Schleife, so dass nur noch 0-en in Ihrem Array stehen.
		for (int i = 0; i < myArray.length; i++) {
			myArray [i] = 0;
			System.out.println (myArray[i]);
		}
		
		
		do {
		//7. Erstellen Sie nun ein Men� mit folgenden Punkten:
		Scanner sc = new Scanner (System.in);
		
		System.out.println("Welchen Fall m�chten Sie ausf�hren? \n a. Alle Werte ausgeben\n b. Einen bestimmten Wert ausgeben\n c. Das Array komplett mit neuen Werten bef�llen\n d. Einen bestimmten Wert �ndern ");
			String a_bis_d = sc.next();
		
		System.out.println("Ihre entscheidung ist: " + a_bis_d);
		
	
		
		//a. Alle Werte ausgeben
		/*if (aufgabe_a.equals("a")){
			System.out.println("hi");
			for (int i = 0; i <	 myArray.length; i++) {
				System.out.println(myArray[i]);
			}
		}
		*/
		
		switch (a_bis_d) {
		case "a":
			for (int i = 0; i <	 myArray.length; i++) {
			System.out.println(myArray[i]);
			}
			break;
		
		//b. Einen bestimmten Wert ausgeben
		case "b":
			System.out.println ("Geben Sie eine Zahl von 0-9 ein: ");
			int ausgabe = sc.nextInt();
			System.out.println(myArray [ausgabe]);
			break;
			
		//c. Das Array komplett mit neuen Werten bef�llen
		case "c":
			myArray[0] = 200;
			myArray[1] = 210;
			myArray[2] = 220;
			myArray[3] = 230;
			myArray[4] = 240;
			myArray[5] = 250;
			myArray[6] = 260;
			myArray[7] = 270;
			myArray[8] = 280;
			myArray[9] = 290;
			for (int i = 0; i < myArray.length; i++) {
				System.out.println (myArray[i]);
			}
			break;
			
		//d. Einen bestimmten Wert �ndern
		case "d":
		System.out.println("Welchen Wert von 0-9 m�chten Sie �ndern?");
		int ausgabe2 = sc.nextInt();
		myArray[ausgabe2] = 69;
		System.out.println ("Der neue Wert betr�gt: " + myArray[ausgabe2]);
		break;
			
			default:
				System.out.println("falsche Eingabe");
				
		}
		//8. Erweitern Sie Ihr Programm von 7. so, dass der Benutzer entscheiden kann, ob es wiederholt werden soll oder nicht.
		System.out.println("Soll Aufgabe 7 wiederholt werden? j/n ");
		wahl = sc.next().charAt(0);
		}
		while(wahl == 'j');
			
			
	
}
}
