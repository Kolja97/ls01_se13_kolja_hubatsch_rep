
public class Ausgabeformatierung_2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Aufgabe 1
				
		System.out.printf(" %10s\n", "**");
		System.out.printf(" %6s      %1s\n", "*","*");
		System.out.printf(" %6s      %1s\n", "*","*");
		System.out.printf(" %10s\n", "**");
		
		
		
		//Aufgabe 2
		
		System.out.printf("0!" + "%3s" + "%19s" + "%7s", "=", "=", "1\n");
		System.out.printf("1!" + "%3s" + "%2s" + "%17s" + "%7s", "=", "1", "=", "1\n");
		System.out.printf("2!" + "%3s" + "%6s" + "%13s" + "%7s", "=", "1 * 2", "=", "2\n");
		System.out.printf("3!" + "%3s" + "%10s" + "%9s" + "%7s", "=", "1 * 2 * 3", "=", "6\n");
		System.out.printf("4!" + "%3s" + "%14s" + "%5s" + "%7s", "=", "1 * 2 * 3 * 4", "=", "24\n");
		System.out.printf("5!" + "%3s" + "%18s" + "%1s" + "%7s", "=", "1 * 2 * 3 * 4 * 5", "=", "120\n\n\n");
		
		
		//Aufgabe 3
		
		System.out.print("Fahrenheit  |  Celsius\n");
		System.out.print("-----------------------\n");
		System.out.printf("%-12s" + "|" + "%10s", "-20", "-28.89\n");
		
		
		
	}

}
