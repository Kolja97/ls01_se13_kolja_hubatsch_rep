/**
 *
 * Beschreibung
 *
 * @version 1.0 vom 24.11.2021
 * @author 
 */


import  java.util.Scanner;
public class Methoden_Wiederholung {
  
  //Deklaration einer Methode
  
  public static int quadrieren (int ganzeZahl){
		       int ergebnis;
		       ergebnis =  ganzeZahl * ganzeZahl;
		       return ergebnis;
    }
  
  
  public static int multiplizieren (int ganzeZahl, int ganzeZahl2){
		       int ergebnis; 
		       ergebnis = ganzeZahl * ganzeZahl2;
		       return ergebnis;
    
    }
  
  public static int addieren (int ganzeZahl, int ganzeZahl3){
			  	int ergebnis;
			  	ergebnis = ganzeZahl + ganzeZahl3;
			  	return ergebnis;
	  	
  }
  

  
  public static void main(String[] args) {
    
										    //Deklaration eines Scanners
										    Scanner myScanner = new Scanner(System.in);
										    
										    int ganzeZahl, erg, ganzeZahl2, ganzeZahl3;
										  
										    
													    System.out.println("Geben Sie eine ganze Zahl ein: ");
													    ganzeZahl = myScanner.nextInt();
													    
															    	erg = quadrieren(ganzeZahl);
															    	System.out.println(ganzeZahl + " hoch 2 = " + erg );
													    
													    	
													    System.out.println("Geben Sie eine zweite ganze Zahl ein, die mit der ersten multipliziert wird: ");
													    ganzeZahl2 = myScanner.nextInt();
													    
															    	erg = multiplizieren(ganzeZahl, ganzeZahl2);
															    	System.out.println(ganzeZahl + " * " + ganzeZahl2 + " = " + erg );
													    
													    	
													    System.out.println("Geben Sie eine Zhal ein um sie mit der ersten zu addieren: ");
													    ganzeZahl3 = myScanner.nextInt();
													    
															    	erg = addieren (ganzeZahl, ganzeZahl3);
															    	System.out.println(ganzeZahl + " + " + ganzeZahl3 + " = " + erg);
										    
										    
										    	
										    myScanner.close ();    
										  } // end of main
  

  } // end of class Methoden_Wiederholung
