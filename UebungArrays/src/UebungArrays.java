
public class UebungArrays {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		int [] myArray = new int [10];
		
		myArray[0] = 100;
		myArray[1] = 110;
		myArray[2] = 120;
		myArray[3] = 130;
		myArray[4] = 140;
		myArray[5] = 150;
		myArray[6] = 160;
		myArray[7] = 170;
		myArray[8] = 180;
		myArray[9] = 190;
		
		for (int i = 0; i < myArray.length ;i++) {
		
			if(myArray[i]%3==0) {
				System.out.println(myArray[i]+ " ist durch 3 teilbar ");
			}
			
		}
		
	}

}
