/**
  *
  * Beschreibung
  *  Einf�hrung in die Programmierung mit Arrays 
  * @version 1.0 vom 19.03.2017
  * @author 
  */
import java.util.Scanner;
public class Materialien_zu_Arrays2 {
  
  public static void main(String[] args) {
	  
	  //keine Ver�nderung
    
    Scanner sc = new Scanner (System.in);
    
    //Deklaration eines Arrays, 5 integer Werte
    //Name des Arrays: "intArray"
    
    int [] intArray = new int [5];
    
    
    //Speichern von Werten im Array
    //Wert 1000 an 3. Stelle des Arrays
    //Wert 500 an Index 4
    
    intArray [2] = 1000;
    intArray [4] = 500;
   // System.out.println (intArray [1]);
    
    
    //Deklaration + Initialisierung eines Arrays in einem Schritt
    //Name des Arrays: "doubleArray"
    // L�nge 3, mit 3 double Werten 
    
    double doubleArray [] = {1.5, 2.5, 3.5}; 
    
    
    
    //Der Wert des double Arrays an Indexposition 1 soll ausgegeben werden
    
    System.out.println (doubleArray [1]);
    
    
    //Nutzer soll einen Index angeben 
    //und an diesen Index einen neuen Wert speichern
    System.out.println("An welchen Index (0-2) soll der neue Wert?");
    Scanner eingabe1 = new Scanner (System.in);
    int neuerIndex = eingabe1.nextInt();
    doubleArray [neuerIndex] = 10;
    System.out.println ("Das ist der neue Wert: " + doubleArray [neuerIndex]);
    
    
    System.out.println("Geben Sie den neuen Wert f�r index " +   1   + " an: ");
    Scanner eingabe2 = new Scanner (System.in);
    double neuerWert = eingabe2.nextInt();
    doubleArray [1] = neuerWert;
    System.out.println("Der neue Wert f�r Index 1 ist: " + neuerWert);
    
    
    
    //Alle Werte vom Array intArray sollen ausgegeben werden
    //Geben Sie zun�chst an, welche Werte Sie in der Ausgabe erwarten: 
    // 0,0,1000,0,500
    for (int i = 0; i < intArray.length; i++) {
		System.out.println (intArray[i]);
	}
    
    
    //Der intArray soll mit neuen Werten gef�llt werden
          //1. alle Felder sollen den Wert 0 erhalten
    
    for (int i = 0; i < intArray.length; i++) {
		intArray[i] = 0;
	}
    for (int i = 0; i < intArray.length; i++) {
		System.out.println (intArray[i]);
	}
    
    
    
    //Der intArray soll mit neuen Werten gef�llt werden
          //2. alle Felder sollen vom Nutzer einen Wert bekommen
          //nutzen Sie dieses Mal die Funktion �length�
    
    for (int i = 0; i < intArray.length; i++) {
    	System.out.println("Welchen Wert soll Index " + i + " haben?: ");
    	Scanner eingabeNutzer = new Scanner (System.in);
    	int neuerWert2 = eingabeNutzer.nextInt();
    	intArray [i] = neuerWert2;
	}
    //zur Kontrolle ob es Funktioniert hat, wird das Array ausgegeben wie es ist:
    for (int i = 0; i < intArray.length; i++) {
		System.out.println (intArray[i]);
	}
    
    
    //Der intArray soll mit neuen Werten gef�llt werden
           //3. Felder automatisch mit folgenden Zahlen f�llen: 10,20,30,40,50
    intArray [0] = 10;
    intArray [1] = 20;
    intArray [2] = 30;
    intArray [3] = 40;
    intArray [4] = 50;
    System.out.println("Die neuen Werte des Array sind: " + intArray [0] + ", " + intArray [1] + ", " + intArray [2] + ", " + intArray [3] + ", " + intArray [4]);
    
    
    
    
    
  } // end of main
  
} // end of class arrayEinfuehrung
