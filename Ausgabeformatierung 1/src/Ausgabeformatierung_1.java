
public class Ausgabeformatierung_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Aufgabe 1
				System.out.print("Das ist ein \"Beispielsatz\"." + "123\n");
				System.out.println("Ein Beispielsatz ist das.");
				
				//das ist ein Kommentar
				
				
		//Aufgabe 2
				System.out.print("        *\n       ***\n      *****\n     *******\n    *********\n   ***********\n  *************\n       ***\n       ***\n");
			
				
		//Aufgabe 3
				double zahl1 = 22.4234234;
				double zahl2 = 111.2222;
				double zahl3 = 4.0;
				double zahl4 = 1000000.551;
				double zahl5 = 97.34;
										
				System.out.printf( "%.2f\n" ,zahl1);
				System.out.printf( "%.2f\n" ,zahl2);
				System.out.printf( "%.2f\n" ,zahl3);
				System.out.printf( "%.2f\n" ,zahl4);
				System.out.printf( "%.2f\n" ,zahl5);
		
		
	}

}
