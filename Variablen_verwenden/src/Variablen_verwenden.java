
public class Variablen_verwenden {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		//package = eingabeAusgabe;

		/** Variablen.java
		    Ergaenzen Sie nach jedem Kommentar jeweils den Quellcode.
		    @author
		    @version
		*/
		
		    /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
		          Vereinbaren Sie eine geeignete Variable */
		int zaehler;

		    /* 2. Weisen Sie dem Zaehler den Wert 25 zu
		          und geben Sie ihn auf dem Bildschirm aus.*/
		zaehler = 25;
		System.out.println(zaehler);

		    /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
		          eines Programms ausgewaehlt werden.
		          Vereinbaren Sie eine geeignete Variable */
		char menue;
		
		//versteh ich nicht

		    /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
		          und geben Sie ihn auf dem Bildschirm aus.*/
		menue = 'c';
		System.out.println(menue);

		    /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
		          notwendig.
		          Vereinbaren Sie eine geeignete Variable */
		long grosserZahlenwert;

		    /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
		          und geben Sie sie auf dem Bildschirm aus.*/
		grosserZahlenwert = 299792458;
		System.out.println(grosserZahlenwert);

		    /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
		          soll die Anzahl der Mitglieder erfasst werden.
		          Vereinbaren Sie eine geeignete Variable und initialisieren sie
		          diese sinnvoll.*/
		short vereinsmitglieder;
		vereinsmitglieder = 7;

		    /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
		System.out.println(vereinsmitglieder);

		    /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
		          Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
		          dem Bildschirm aus.*/
		final double eElWert = 1.602E-19;
		System.out.println (eElWert);

		    /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
		          Vereinbaren Sie eine geeignete Variable. */
		double Zahlung;

		    /*11. Die Zahlung ist erfolgt.
		          Weisen Sie der Variable den entsprechenden Wert zu
		          und geben Sie die Variable auf dem Bildschirm aus.*/
		Zahlung = 7000000;
		System.out.println(Zahlung);

		  }//main
		}// Variablen